# Déclaration du provider AWS
provider "aws" {
  region = "eu-west-3"
}

resource "aws_security_group" "jenkins_security_group" {
  name        = "jenkins-security-group"
  description = "Security group for Jenkins"

  # SSH
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTP
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # HTTPS
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    app = "jenkins"
  }
}

resource "aws_instance" "jenkins_instance" {
  ami           = "ami-087da76081e7685da"
  instance_type = "t3.micro"
  key_name      = "aws-ec2"
  security_groups = [aws_security_group.jenkins_security_group.name]

  tags = {
    app = "jenkins"
  }

  provisioner "local-exec" {
    command = "echo ${self.public_ip} > ip.txt"
  }
}

